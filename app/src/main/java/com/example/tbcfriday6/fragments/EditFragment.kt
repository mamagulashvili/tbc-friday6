package com.example.tbcfriday6.fragments

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.tbcfriday6.R
import com.example.tbcfriday6.data.ItemModel
import com.example.tbcfriday6.databinding.EditFragmentBinding

class EditFragment : Fragment() {
    private var _binding:EditFragmentBinding? = null
    private val binding get() = _binding!!
    val args:EditFragmentArgs by navArgs()
    private val viewModel:EditViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = EditFragmentBinding.inflate(layoutInflater,container,false)
        init()
        return binding.root
    }

    private fun init(){
        binding.etTitle.setText(args.item.title)
        binding.etDescription.setText(args.item.description)
        binding.btnEdit.setOnClickListener {
            editItem()
        }
    }
    private fun editItem(){
        val title = binding.etTitle.text.toString()
        val desc = binding.etDescription.text.toString()
        if (title.isNotEmpty() && desc.isNotEmpty()){
            val updatedItem = ItemModel(args.item.id,title,desc,args.item.pictureUrl)
            viewModel.editItem(updatedItem)
        }
    }
}