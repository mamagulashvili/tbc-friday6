package com.example.tbcfriday6.data

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize


@Entity(tableName = "item_table")
@Parcelize
data class ItemModel(
    @PrimaryKey(autoGenerate = true)
    val id:Int,
    val title:String?,
    val description:String?,
    val pictureUrl:String?
):Parcelable
