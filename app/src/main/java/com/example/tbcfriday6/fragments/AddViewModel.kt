package com.example.tbcfriday6.fragments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tbcfriday6.data.Database
import com.example.tbcfriday6.data.ItemModel
import kotlinx.coroutines.launch

class AddViewModel(
) : ViewModel() {
    fun insertItem(itemModel: ItemModel) = viewModelScope.launch {
        Database.database.itemDao().insertItem(itemModel)
    }
}