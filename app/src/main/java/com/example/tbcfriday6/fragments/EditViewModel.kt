package com.example.tbcfriday6.fragments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tbcfriday6.data.Database
import com.example.tbcfriday6.data.ItemModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class EditViewModel : ViewModel() {
    fun editItem(itemModel: ItemModel) = viewModelScope.launch (Dispatchers.IO){
        Database.database.itemDao().updateItem(itemModel)
    }
}