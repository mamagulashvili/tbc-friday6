package com.example.tbcfriday6

import com.example.tbcfriday6.data.ItemDatabase
import com.example.tbcfriday6.data.ItemModel

class MainRepository(
    val db:ItemDatabase
) {
    suspend fun insetItem(itemModel: ItemModel) = db.itemDao().insertItem(itemModel)
    fun getAllItem() = db.itemDao().getAllItem()
}