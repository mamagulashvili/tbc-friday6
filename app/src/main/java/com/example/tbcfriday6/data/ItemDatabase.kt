package com.example.tbcfriday6.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [ItemModel::class], version = 2)

abstract class ItemDatabase : RoomDatabase() {
    abstract fun itemDao(): ItemDao
}