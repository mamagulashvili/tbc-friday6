package com.example.tbcfriday6.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.tbcfriday6.R
import com.example.tbcfriday6.data.ItemModel
import com.example.tbcfriday6.databinding.AddFragmentBinding

class AddFragment : Fragment() {
    private var _binding: AddFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: AddViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = AddFragmentBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        binding.btnSave.setOnClickListener {
            setData()

        }
    }

    private fun setData() {
        val imageUrlList = mutableListOf(
            "https://cdn.vox-cdn.com/thumbor/G_V3waTUOGXyOYV7wnazAwVYjWI=/0x0:1943x2720/1200x800/filters:focal(868x628:1178x938)/cdn.vox-cdn.com/uploads/chorus_image/image/69424753/1232858943.0.jpg",
            "https://upload.wikimedia.org/wikipedia/en/5/53/Arsenal_FC.svg",
            "https://images2.minutemediacdn.com/image/upload/c_fill,w_720,ar_16:9,f_auto,q_auto,g_auto/shape/cover/sport/Arsenal-v-Olympiacos---UEFA-Europa-League-Round-Of-a4f3cfabeab2b136ff257efbe373cc93.jpg",
            "https://a.espncdn.com/photo/2020/1228/r795527_2_1296x729_16-9.jpg"
        )
        val imageUrl = imageUrlList.random()
        val title = binding.etTitle.text.toString()
        val description = binding.etDescription.text.toString()
        if (title.isNotEmpty() && description.isNotEmpty()) {
            if (title.length in 6..29 && description.length in 32..300){
                val item = ItemModel(0, title, description, imageUrl)
                viewModel.insertItem(item)
                Toast.makeText(requireContext(), "successfully", Toast.LENGTH_SHORT).show()
                findNavController().navigate(R.id.action_addFragment_to_mainFragment)
            }else{
                Toast.makeText(requireContext(), "inputs are short", Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(requireContext(), "fill all field", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}