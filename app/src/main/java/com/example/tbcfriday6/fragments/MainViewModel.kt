package com.example.tbcfriday6.fragments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tbcfriday6.MainRepository
import com.example.tbcfriday6.data.Database
import com.example.tbcfriday6.data.ItemDatabase
import com.example.tbcfriday6.data.ItemModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel() : ViewModel() {

    fun getAllItem() = Database.database.itemDao().getAllItem()
    fun deleteItem(itemModel: ItemModel) = viewModelScope.launch(Dispatchers.IO) {
        Database.database.itemDao().deleteItem(itemModel)
    }
}