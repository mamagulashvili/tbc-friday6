package com.example.tbcfriday6.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertItem(itemModel: ItemModel)

    @Delete
    suspend fun deleteItem(itemModel: ItemModel)

    @Query("SELECT * FROM item_table")
    fun getAllItem():LiveData<List<ItemModel>>

    @Update
    suspend fun updateItem(itemModel: ItemModel)
}