package com.example.tbcfriday6.data

import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.tbcfriday6.App

object Database {

    val database:ItemDatabase by lazy {
        Room.databaseBuilder(
            App.context!!,
            ItemDatabase::class.java,
            "item_db"
        ).build()
    }
}