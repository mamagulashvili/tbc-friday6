package com.example.tbcfriday6.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tbcfriday6.ItemAdapter
import com.example.tbcfriday6.OnButtonClick
import com.example.tbcfriday6.R
import com.example.tbcfriday6.databinding.MainFragmentBinding

class MainFragment : Fragment() {

    private var _binding: MainFragmentBinding? = null
    private val binding get() = _binding!!
    var isFirst = true

    private val viewModel: MainViewModel by viewModels()
    private lateinit var itemAdapter: ItemAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = MainFragmentBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        setRecycle()
        if (isFirst) {
            binding.txtError.visibility = View.VISIBLE
        } else {
            binding.txtError.visibility = View.GONE
        }
        binding.fabAddUser.setOnClickListener {
            isFirst = false
            findNavController().navigate(R.id.action_mainFragment_to_addFragment)
        }
        observe()
    }

    private fun observe() {
        viewModel.getAllItem().observe(viewLifecycleOwner, {
            itemAdapter.setData(it.toMutableList())
        })
    }

    private fun setRecycle() {
        val onButtonClick = object : OnButtonClick {
            override fun onEditClick(position: Int) {
                editItem(position)
            }

            override fun onDeleteClick(position: Int) {
                deleteItem(position)
            }

        }
        itemAdapter = ItemAdapter(onButtonClick)
        binding.rvItem.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = itemAdapter
        }
    }
    private fun editItem(position: Int){
        val item = itemAdapter.itemList[position]

        val action = MainFragmentDirections.actionMainFragmentToEditFragment(item)
        Navigation.findNavController(binding.root).navigate(action)

    }
    private fun deleteItem(position: Int) {
        val item = itemAdapter.itemList[position]
        viewModel.deleteItem(item)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}