package com.example.tbcfriday6

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tbcfriday6.data.ItemModel
import com.example.tbcfriday6.databinding.RowItemBinding

class ItemAdapter(val onButtonClick: OnButtonClick) : RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {

    var itemList = mutableListOf<ItemModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            RowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int = itemList.size


    inner class ItemViewHolder(val binding: RowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {
            val item = itemList[adapterPosition]
            binding.apply {
                tvTitle.text = item.title
                tvDescription.text = item.description
            }
            itemView.apply {
                Glide.with(this).load(item.pictureUrl).into(binding.ivPicture)
            }
            binding.btnDelete.setOnClickListener {
                onButtonClick.onDeleteClick(adapterPosition)
            }
            binding.btnEdit.setOnClickListener {
                onButtonClick.onEditClick(adapterPosition)
            }
        }
    }
    fun setData(list:MutableList<ItemModel>){
        this.itemList = list
        notifyDataSetChanged()
    }
}