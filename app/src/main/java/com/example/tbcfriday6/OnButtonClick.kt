package com.example.tbcfriday6

interface OnButtonClick {

    fun onEditClick(position:Int)
    fun onDeleteClick(position: Int)
}